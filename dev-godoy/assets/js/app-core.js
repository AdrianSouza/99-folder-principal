(function ($) {

    "use strict";

    window.appCore = {
        /**
         * Configurations
         */
        settings: {
            assets_uri: "./assets/images/",
            btn_slider_uri: "https://motoristas.99pop.com.br/seja-motorista-pop-ggl-src-sp/",
        },

        /**
         * Main View
         */
        driverView: "#user-view",
        /**
         * Main Form
         */
        driverForm: "#new_driver",

        /**
         * Main Minor
         */
        mainMinor: "#menor_idade",

        /**
         * Main Adult
         */
        mainAdult: "#maior_idade",

        /**
         * Btn Simulate Now
         */
        btnSimulateNow: ".btn-simulate-now",

        /**
         * Btn Nav Previous
         */
        btnPrevious: "li.previous a",

        /**
         * Btn Nav Next
         */
        btnNext: "li.next a",

        /**
         * Step 1
         * Driver Radio Age
         */
        driverAge: "driver[maior_idade]",

        /**
         * Step 2
         * Driver Radio Is Driver
         */
        driver99: "driver[motorista_99]",

        /**
         * Step 2
         * Driver Radio City ID
         */
        driverCity: "driver[city_id]",

        /**
         * Step 3
         * Driver Radio Days
         */
        driverDays: ".days",

        /**
         * Step 3
         * Driver Select Hours
         */
        driverHours: "driver[horas_dia]",

        /**
         * Step 4
         * Driver Radio Car
         */
        driveCar: "driver[carro_proprio]",

        /**
         * Step 4
         * Driver Select Fuel ID
         */
        driverFuel: "driver[fuel_id]",

        /**
         * Step 4
         * Driver Select Spent Average
         */
        driverSpent: "driver[consumo_medio_combustivel]",

        /**
         * Step 5
         * Driver Radio Insurance
         */
        driveInsurance: "driver[seguro]",

        /**
         * Step 5
         * Driver Select Wash
         */
        driverWash: "driver[custo_lavagem_mes]",

        /**
         * Step 5
         * Driver Select Food
         */
        driverFood: "driver[custo_alimentacao_dia]",

        /**
         * Btn Submit
         */
        btnSubmit: "input[name='commit']",

        /**
         * Start Controller
         */
        startController: function () {

            /**
             * Mains
             * @type {*|jQuery|HTMLElement}
             */
            appCore.driverView = $(appCore.driverView);
            appCore.driverForm = $(appCore.driverForm);
            appCore.mainMinor = $(appCore.mainMinor);
            appCore.mainAdult = $(appCore.mainAdult);

            /**
             * Nav
             * @type {*|jQuery|HTMLElement}
             */
            appCore.btnPrevious = $(appCore.btnPrevious);
            appCore.btnNext = $(appCore.btnNext);

            /**
             * Inputs Radio | Selects
             * @type {*|jQuery|HTMLElement}
             */
            appCore.driverAge = $("input[name='" + appCore.driverAge + "']");
            appCore.driver99 = $("input[name='" + appCore.driver99 + "']");
            appCore.driverCity = $("select[name='" + appCore.driverCity + "']");
            appCore.driverDays = $(appCore.driverDays).find("input[type='radio']");
            appCore.driverHours = $("select[name='" + appCore.driverHours + "']");
            appCore.driveCar = $("input[name='" + appCore.driveCar + "']");
            appCore.driverFuel = $("select[name='" + appCore.driverFuel + "']");
            appCore.driverSpent = $("select[name='" + appCore.driverSpent + "']");
            appCore.driveInsurance = $("input[name='" + appCore.driveInsurance + "']");
            appCore.driverWash = $("select[name='" + appCore.driverWash + "']");
            appCore.driverFood = $("select[name='" + appCore.driverFood + "']");

            /**
             * Buttons
             * @type {*|jQuery|HTMLElement}
             */
            appCore.btnSimulateNow = $(appCore.btnSimulateNow);
            appCore.btnSubmit = $(appCore.btnSubmit);

            /**
             * Preload Backgrounds
             */
            let bkgs = ["body-bkg", "step-1-no/body-bkg-step-1-no"];
            for (let i = 1; i <= 6; i++) {
                bkgs.push("step-" + i + "/body-bkg-step-" + i);
            }
            $(bkgs).preload();

            /**
             * Heights|Potions elements corrections
             */
            let footer = $(".content-form-footer");
            let offset = footer.offset();
            /**
             * Height Child Containers
             */
            $(".content-start, .step").css({
                height: $("html").height() - $(".content-form-header").height() - footer.height()
            });

            /**
             * Height content
             */
            $(".content-start .content").css({
                height: $(".content-start .content img.right").outerHeight()
            });

            /**
             * Nav bottom position
             */
            let nav = $("nav");
            nav.css({
                top: offset.top - 40
            });

            /**
             * Step 1
             */
            $(".center-step-1").css({
                bottom: footer.height() - 40,
                display: "none"
            });

            /**
             * Step 2
             */
            $(".center-step-2").css({
                bottom: footer.height() - 70,
                display: "none"
            });
            /**
             * Load Bind Events
             * @type Function
             */
            appCore.bindEvents();
        },

        /**
         * Bind Events Functions
         */
        bindEvents: function () {

            /**
             * Overlay or alert area click is exit
             */
            $(".overlay, .accept-alert").on("click", function () {
                $(".overlay, .accept-alert").fadeOut(600);
            });
            /**
             * Slide button
             */
            let btn99 = new Dragdealer('btn99', {
                steps: 3,
                callback: function (x, y) {
                    console.log(x);

                    if (x >= 0.5) {
                        this.disable();
                        $('#btn99').fadeOut();
                        setTimeout(function () {
                            btn99.enable();
                            btn99.setValue(0, 0, true);
                            $('#btn99').fadeIn();
                            window.location.href = appCore.settings.btn_slider_uri;
                        }, 300);
                    }
                }
            });

            /**
             * Navigator
             */
            appCore.btnPrevious.on("click", function () {
                let cur_step = $("body").data("step");

                /**
                 * If the step is less than the first existing reload app to first step
                 */
                if (cur_step <= 1) location.reload();

                /**
                 * Current step element
                 * @type {*|jQuery|HTMLElement}
                 */
                let step = $(".step-" + cur_step);
                /**
                 * Load previous step
                 */
                step.fadeOut(400, function () {
                    appCore.prepareStepScreen((parseInt(cur_step) - 1));
                });
            });

            /**
             * Step Start
             * Action Simulate Now
             */
            appCore.btnSimulateNow.on("click", function () {
                $(".accept-alert").centerTop().find(".btn-accept-alert").on("click", function () {
                    $(".overlay, .content-start").fadeOut(400, function () {
                        /**
                         * Show step screen
                         */
                        appCore.prepareStepScreen(1);
                        /**
                         * Show Parent
                         */
                        appCore.driverForm.parent().fadeIn(400);
                    })
                });
                /**
                 * Hide Overlay and Alert
                 */
                $(".overlay, .accept-alert").fadeIn(400);
            });

            /**
             * Step 1
             * Show Hide Minor/Adult
             */
            appCore.driverAge.on("click", appCore.driverForm, function () {
                let _this = $(this);
                /**
                 * Clear all and change current background to #e6e7e8
                 */
                _this.closest("div.container").find("span.radio").removeClass("selected");
                _this.closest("span").toggleClass("selected");

                setTimeout(function () {
                    /**
                     * If true
                     */
                    if (_this.val().is_true) {
                        appCore.mainMinor.fadeOut(400, function () {
                            appCore.mainAdult.fadeIn(400);
                            appCore.prepareStepScreen(2);
                        });
                        return true;
                    }
                    /**
                     * Step 1 NO
                     * Default (if false)
                     */
                    $(".step-1").fadeOut(400, function () {
                        appCore.driverView.css({
                            backgroundImage: "url('" + appCore.settings.assets_uri + "step-1-no/body-bkg-step-1-no.jpg')",
                            backgroundPositionY: -10,
                            height: appCore.mainMinor.height() + $(".bottom-hands").height()
                        });
                        $("nav, .content-form-footer, .center-step-1").remove();

                        appCore.mainMinor.fadeIn(400, function () {
                            appCore.preventDefauls();
                        }).append("<div class='content-form-footer'> <a href='http://onelink.to/afanz3' class='download center-hor'></a> </div>");
                    });
                }, 1000);


            });

            /**
             * Step 2
             */
            appCore.driver99.on("click", appCore.driverForm, function () {
                /**
                 * Clear all and change current background to #e6e7e8
                 */
                $(this).closest("div.container").find("span.radio").removeClass("selected");
                $(this).closest("span").toggleClass("selected");
                /**
                 * Check if selected is valid
                 */
                if (appCore.driver99.is(':checked')) {
                    appCore.driverCity.prop('disabled', false);
                }
            });

            appCore.driverCity.on("change", appCore.driverForm, function () {
                if (parseInt($(this).find(":selected").val()) !== 0) {
                    $(".step-2").fadeOut(400, function () {
                        appCore.prepareStepScreen(3);
                    });
                }
            });

            /**
             * Step 3
             */
            appCore.driverDays.on("click", appCore.driverForm, function () {
                let _this = $(this);

                /**
                 * Clear checked and change current background to #e6e7e8
                 */
                _this.prop('checked', false);
                _this.closest("span").toggleClass("selected");

                /**
                 * Set checked if is selected
                 */
                if (_this.closest("span").hasClass("selected")) $(this).prop('checked', true);

                /**
                 * Check if only one is checked if true enable drop hours (appCore.driverHours)
                 */
                if (appCore.driverDays.filter(":checked").length) {
                    appCore.driverHours.prop('disabled', false);
                    return true;
                }
                appCore.driverHours.prop('disabled', true);
            });

            appCore.driverHours.on("change", appCore.driverForm, function () {
                /**
                 * Check if selected is valid
                 */
                if (parseInt($(this).find(":selected").val()) !== 0) {
                    $(".step-3").fadeOut(400, function () {
                        appCore.prepareStepScreen(4);
                    });

                }
            });

            /**
             * Step 4
             */
            appCore.driveCar.on("change", appCore.driverForm, function () {
                $(this).closest("div.container").find("span.radio").removeClass("selected");
                $(this).closest("span").toggleClass("selected");
                if (appCore.driveCar.is(':checked')) {
                    appCore.driverFuel.prop('disabled', false);
                }
            });
            appCore.driverFuel.on("change", appCore.driverForm, function () {
                if (parseInt($(this).find(":selected").val()) !== 0) {
                    appCore.driverSpent.prop('disabled', false);
                    return true;
                }
                appCore.driverSpent.prop('disabled', true);
            });
            appCore.driverSpent.on("change", appCore.driverForm, function () {
                if (appCore.driveCar.is(':checked') && appCore.driverFuel.find(":selected").val() !== 0) {
                    $(".step-4").fadeOut(400, function () {
                        appCore.prepareStepScreen(5);
                    });
                }
            });

            /**
             * Step 5
             */
            appCore.driveInsurance.on("change", appCore.driverForm, function () {
                $(this).closest("div.container").find("span.radio").removeClass("selected");
                $(this).closest("span").toggleClass("selected");
                if (appCore.driveInsurance.is(':checked')) {
                    appCore.driverWash.prop('disabled', false);
                }
            });
            appCore.driverWash.on("change", appCore.driverForm, function () {
                if (parseInt($(this).find(":selected").val()) !== 0) {
                    appCore.driverFood.prop('disabled', false);
                    return true;
                }
                appCore.driverFood.prop('disabled', true);
            });
            appCore.driverFood.on("change", appCore.driverForm, function () {
                if (appCore.driveInsurance.is(':checked') && appCore.driverWash.find(":selected").val() !== 0) {
                    appCore.btnSubmit.fadeIn(600);
                }
            });

            /**
             * Step 5 Success
             */
            appCore.btnSubmit.on("click", appCore.driverForm, function (event) {
                event.preventDefault();

                appCore.driverHours.prop('disabled', false);
                appCore.driverFuel.prop('disabled', false);
                appCore.driverSpent.prop('disabled', false);
                appCore.driverCity.prop('disabled', false);
                appCore.driverWash.prop('disabled', false);
                appCore.driverFood.prop('disabled', false);

                let data = JSON.stringify(appCore.driverForm.serializeArray());
                console.log(JSON.parse(data));

                /**
                 * Submit form
                 */
                //appCore.driverForm.submit();

                $(".step-5").fadeOut(400, function () {
                    appCore.driverView.css({
                        backgroundImage: "url('" + appCore.settings.assets_uri + "step-6/body-bkg-step-6.jpg')",
                        backgroundPositionY: -10,
                        height: $(".success").height() + $(".bottom-hands").height() + 210
                    });
                    $("nav, .center-step-1, .center-step-2, .content-form").remove();

                    $(".success").fadeIn(400, function () {
                        appCore.preventDefauls();
                    });
                });

            })
        },

        resetFormPart: function (element) {
            $(element).find("input[type='radio']").each(function (i) {
                $(this).prop('checked', false).closest("div.container").find("span.radio").removeClass("selected");
            });
            $(element).find("select").each(function (i) {
                $(this).prop('selectedIndex', 0);
            })
        },

        /**
         * Prepare Screen to Load
         * @type function
         * @param step
         * @param callback
         */
        prepareStepScreen: function (step, callback) {

            /**
             * Save current step
             */
            $("body").data("step", step);

            /**
             * Reset Step
             */
            appCore.resetFormPart(".step-" + step);

            /**
             * Load background first
             * To not blink screen
             */
            appCore.driverView.css({
                backgroundImage: "url('" + appCore.settings.assets_uri + "step-" + step + "/body-bkg-step-" + step + ".jpg')"
            });

            /**
             * Prevent Defaults
             */
            appCore.preventDefauls();
            /**
             * Hide all step
             * Show current step
             */
            $(".step").fadeOut(400, function () {

                /**
                 * Prepare animate children
                 * Opacity 0
                 */
                $(this).children().each(function () {
                    $(this).css({
                        opacity: 0
                    });
                });
                /**
                 * Show Center Images absolute
                 */
                $('*[class^="center-step"]').hide(0)
                if ($(".center-step-" + step).length) $(".center-step-" + step).show(0);

                /**
                 * Step show
                 */
                $(".step-" + step).fadeIn(300, function () {
                    /**
                     * Animate children
                     * Opacity 1
                     */
                    $(".step-" + step).children().each(function () {
                        $(this).animate({
                            opacity: 1
                        }, 600);
                    });

                    /**
                     * Active Previous Button
                     */
                    appCore.btnPrevious.fadeIn(600);

                    /**
                     * Active Next Button
                     *
                     * if(step === 3 || step === 4) appCore.btnNext.fadeIn(600);
                     */

                    /**
                     * Run callback
                     */
                    if (typeof callback === 'function') {
                        callback();
                    }
                });
            });


        },

        /**
         * Prevent Defaults
         */
        preventDefauls: function () {
            appCore.btnNext.fadeOut(200);
            appCore.btnSubmit.fadeOut(200);
            appCore.driverHours.prop('disabled', true);
            appCore.driverFuel.prop('disabled', true);
            appCore.driverSpent.prop('disabled', true);
            appCore.driverCity.prop('disabled', true);
            appCore.driverWash.prop('disabled', true);
            appCore.driverFood.prop('disabled', true);
        },
    };

    /**
     * Init Controller When Document is Ready!
     */
    $(document).ready(function () {
        appCore.startController();
    });

    /**
     * Check if string is true as boolean
     * Object String Prototype
     * Regex ^(true|1)$
     */
    Object.defineProperty(String.prototype, "is_true", {
        get: function () {
            return (/^(true|1)$/i).test(this);
        }
    });

    /**
     * Center element in screen:(y)
     * An object to merge onto the jQuery prototype.
     * @type Object
     * @see jQuery.fn.extend( object )
     */
    $.fn.extend({
        centerTop: function () {
            return this.each(function () {
                let top = ($(window).height() - $(this).outerHeight()) / 2;
                $(this).css({position: 'absolute', margin: 0, top: (top > 0 ? top : 0) + 'px'});
            });
        }
    });

    /**
     * Preload Images
     * An object to merge onto the jQuery prototype.
     * @type Object
     * @see jQuery.fn.extend( object )
     */
    $.fn.preload = function () {
        this.each(function () {
            $('<img/>')[0].src = appCore.settings.assets_uri + this + ".jpg";
        });
    }
})(jQuery);
