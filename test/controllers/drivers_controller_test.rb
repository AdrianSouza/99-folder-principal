require 'test_helper'

class DriversControllerTest < ActionDispatch::IntegrationTest
  setup do
    @driver = drivers(:one)
  end

  test "should get index" do
    get drivers_url
    assert_response :success
  end

  test "should get new" do
    get new_driver_url
    assert_response :success
  end

  test "should create driver" do
    assert_difference('Driver.count') do
      post drivers_url, params: { driver: { carro_proprio: @driver.carro_proprio, celular: @driver.celular, consumo_medio_combustivel: @driver.consumo_medio_combustivel, custo_alimentacao_dia: @driver.custo_alimentacao_dia, custo_lavagem_mes: @driver.custo_lavagem_mes, email: @driver.email, horas_dia: @driver.horas_dia, maior_idade: @driver.maior_idade, motorista_99: @driver.motorista_99, qtd_dias_semana: @driver.qtd_dias_semana, quais_dias_semana: @driver.quais_dias_semana, seguro: @driver.seguro } }
    end

    assert_redirected_to driver_url(Driver.last)
  end

  test "should show driver" do
    get driver_url(@driver)
    assert_response :success
  end

  test "should get edit" do
    get edit_driver_url(@driver)
    assert_response :success
  end

  test "should update driver" do
    patch driver_url(@driver), params: { driver: { carro_proprio: @driver.carro_proprio, celular: @driver.celular, consumo_medio_combustivel: @driver.consumo_medio_combustivel, custo_alimentacao_dia: @driver.custo_alimentacao_dia, custo_lavagem_mes: @driver.custo_lavagem_mes, email: @driver.email, horas_dia: @driver.horas_dia, maior_idade: @driver.maior_idade, motorista_99: @driver.motorista_99, qtd_dias_semana: @driver.qtd_dias_semana, quais_dias_semana: @driver.quais_dias_semana, seguro: @driver.seguro } }
    assert_redirected_to driver_url(@driver)
  end

  test "should destroy driver" do
    assert_difference('Driver.count', -1) do
      delete driver_url(@driver)
    end

    assert_redirected_to drivers_url
  end
end
