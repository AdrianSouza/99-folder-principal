require 'test_helper'

class ConstsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @const = consts(:one)
  end

  test "should get index" do
    get consts_url
    assert_response :success
  end

  test "should get new" do
    get new_const_url
    assert_response :success
  end

  test "should create const" do
    assert_difference('Const.count') do
      post consts_url, params: { const: { depreciacao: @const.depreciacao, ipva_dpvat_licenciamento: @const.ipva_dpvat_licenciamento, manutencao: @const.manutencao, mensal: @const.mensal, seguro: @const.seguro, total: @const.total } }
    end

    assert_redirected_to const_url(Const.last)
  end

  test "should show const" do
    get const_url(@const)
    assert_response :success
  end

  test "should get edit" do
    get edit_const_url(@const)
    assert_response :success
  end

  test "should update const" do
    patch const_url(@const), params: { const: { depreciacao: @const.depreciacao, ipva_dpvat_licenciamento: @const.ipva_dpvat_licenciamento, manutencao: @const.manutencao, mensal: @const.mensal, seguro: @const.seguro, total: @const.total } }
    assert_redirected_to const_url(@const)
  end

  test "should destroy const" do
    assert_difference('Const.count', -1) do
      delete const_url(@const)
    end

    assert_redirected_to consts_url
  end
end
