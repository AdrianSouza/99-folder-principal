require 'test_helper'

class CitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @city = cities(:one)
  end

  test "should get index" do
    get cities_url
    assert_response :success
  end

  test "should get new" do
    get new_city_url
    assert_response :success
  end

  test "should create city" do
    assert_difference('City.count') do
      post cities_url, params: { city: { asp: @city.asp, distancia_corrida: @city.distancia_corrida, distancia_pax: @city.distancia_pax, name: @city.name, rs_km: @city.rs_km, rs_min: @city.rs_min, tarifa_base: @city.tarifa_base, tarifa_minima: @city.tarifa_minima, taxa_99: @city.taxa_99, tempo_medio_corrida: @city.tempo_medio_corrida, tph: @city.tph } }
    end

    assert_redirected_to city_url(City.last)
  end

  test "should show city" do
    get city_url(@city)
    assert_response :success
  end

  test "should get edit" do
    get edit_city_url(@city)
    assert_response :success
  end

  test "should update city" do
    patch city_url(@city), params: { city: { asp: @city.asp, distancia_corrida: @city.distancia_corrida, distancia_pax: @city.distancia_pax, name: @city.name, rs_km: @city.rs_km, rs_min: @city.rs_min, tarifa_base: @city.tarifa_base, tarifa_minima: @city.tarifa_minima, taxa_99: @city.taxa_99, tempo_medio_corrida: @city.tempo_medio_corrida, tph: @city.tph } }
    assert_redirected_to city_url(@city)
  end

  test "should destroy city" do
    assert_difference('City.count', -1) do
      delete city_url(@city)
    end

    assert_redirected_to cities_url
  end
end
