require "application_system_test_case"

class ConstsTest < ApplicationSystemTestCase
  setup do
    @const = consts(:one)
  end

  test "visiting the index" do
    visit consts_url
    assert_selector "h1", text: "Consts"
  end

  test "creating a Const" do
    visit consts_url
    click_on "New Const"

    fill_in "Depreciacao", with: @const.depreciacao
    fill_in "Ipva dpvat lic", with: @const.ipva_dpvat_lic
    fill_in "Manutencao", with: @const.manutencao
    fill_in "Mensal", with: @const.mensal
    fill_in "Seguro", with: @const.seguro
    fill_in "Total", with: @const.total
    click_on "Create Const"

    assert_text "Const was successfully created"
    click_on "Back"
  end

  test "updating a Const" do
    visit consts_url
    click_on "Edit", match: :first

    fill_in "Depreciacao", with: @const.depreciacao
    fill_in "Ipva dpvat lic", with: @const.ipva_dpvat_lic
    fill_in "Manutencao", with: @const.manutencao
    fill_in "Mensal", with: @const.mensal
    fill_in "Seguro", with: @const.seguro
    fill_in "Total", with: @const.total
    click_on "Update Const"

    assert_text "Const was successfully updated"
    click_on "Back"
  end

  test "destroying a Const" do
    visit consts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Const was successfully destroyed"
  end
end
