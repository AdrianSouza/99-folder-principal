require "application_system_test_case"

class DriversTest < ApplicationSystemTestCase
  setup do
    @driver = drivers(:one)
  end

  test "visiting the index" do
    visit drivers_url
    assert_selector "h1", text: "Drivers"
  end

  test "creating a Driver" do
    visit drivers_url
    click_on "New Driver"

    fill_in "Carro proprio", with: @driver.carro_proprio
    fill_in "Celular", with: @driver.celular
    fill_in "Consumo medio combustivel", with: @driver.consumo_medio_combustivel
    fill_in "Custo alimentacao dia", with: @driver.custo_alimentacao_dia
    fill_in "Custo lavagem mes", with: @driver.custo_lavagem_mes
    fill_in "Email", with: @driver.email
    fill_in "Horas dia", with: @driver.horas_dia
    fill_in "Maior idade", with: @driver.maior_idade
    fill_in "Motorista 99", with: @driver.motorista_99
    fill_in "Qtd dias semana", with: @driver.qtd_dias_semana
    fill_in "Quais dias semana", with: @driver.quais_dias_semana
    fill_in "Seguro", with: @driver.seguro
    click_on "Create Driver"

    assert_text "Driver was successfully created"
    click_on "Back"
  end

  test "updating a Driver" do
    visit drivers_url
    click_on "Edit", match: :first

    fill_in "Carro proprio", with: @driver.carro_proprio
    fill_in "Celular", with: @driver.celular
    fill_in "Consumo medio combustivel", with: @driver.consumo_medio_combustivel
    fill_in "Custo alimentacao dia", with: @driver.custo_alimentacao_dia
    fill_in "Custo lavagem mes", with: @driver.custo_lavagem_mes
    fill_in "Email", with: @driver.email
    fill_in "Horas dia", with: @driver.horas_dia
    fill_in "Maior idade", with: @driver.maior_idade
    fill_in "Motorista 99", with: @driver.motorista_99
    fill_in "Qtd dias semana", with: @driver.qtd_dias_semana
    fill_in "Quais dias semana", with: @driver.quais_dias_semana
    fill_in "Seguro", with: @driver.seguro
    click_on "Update Driver"

    assert_text "Driver was successfully updated"
    click_on "Back"
  end

  test "destroying a Driver" do
    visit drivers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Driver was successfully destroyed"
  end
end
