require "application_system_test_case"

class CitiesTest < ApplicationSystemTestCase
  setup do
    @city = cities(:one)
  end

  test "visiting the index" do
    visit cities_url
    assert_selector "h1", text: "Cities"
  end

  test "creating a City" do
    visit cities_url
    click_on "New City"

    fill_in "Asp", with: @city.asp
    fill_in "Distancia corrida", with: @city.distancia_corrida
    fill_in "Distancia pax", with: @city.distancia_pax
    fill_in "Name", with: @city.name
    fill_in "Rs km", with: @city.rs_km
    fill_in "Rs min", with: @city.rs_min
    fill_in "Tarifa base", with: @city.tarifa_base
    fill_in "Tarifa minima", with: @city.tarifa_minima
    fill_in "Taxa 99", with: @city.taxa_99
    fill_in "Tempo medio corrida", with: @city.tempo_medio_corrida
    fill_in "Tph", with: @city.tph
    click_on "Create City"

    assert_text "City was successfully created"
    click_on "Back"
  end

  test "updating a City" do
    visit cities_url
    click_on "Edit", match: :first

    fill_in "Asp", with: @city.asp
    fill_in "Distancia corrida", with: @city.distancia_corrida
    fill_in "Distancia pax", with: @city.distancia_pax
    fill_in "Name", with: @city.name
    fill_in "Rs km", with: @city.rs_km
    fill_in "Rs min", with: @city.rs_min
    fill_in "Tarifa base", with: @city.tarifa_base
    fill_in "Tarifa minima", with: @city.tarifa_minima
    fill_in "Taxa 99", with: @city.taxa_99
    fill_in "Tempo medio corrida", with: @city.tempo_medio_corrida
    fill_in "Tph", with: @city.tph
    click_on "Update City"

    assert_text "City was successfully updated"
    click_on "Back"
  end

  test "destroying a City" do
    visit cities_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "City was successfully destroyed"
  end
end
