class Driver < ApplicationRecord
  belongs_to :city
  belongs_to :fuel
  belongs_to :rent
  belongs_to :const

  validates :seguro, presence: true

  validates :maior_idade, presence: true
  validates :city, presence: true
  validates :qtd_dias_semana, presence: true
  validates :horas_dia, presence: true
  validates :fuel, presence: true
  validates :consumo_medio_combustivel, presence: true
  validates :custo_lavagem_mes, presence: true
  validates :custo_alimentacao_dia, presence: true
end
