class FuelsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_fuel, only: [:show, :edit, :update, :destroy]

  def index
    @fuels = Fuel.all
  end

  def show
  end

  def new
    @fuel = Fuel.new
  end

  def edit
  end

  def create
    @fuel = Fuel.new(fuel_params)

    respond_to do |format|
      if @fuel.save
        format.html { redirect_to @fuel, notice: 'criado.' }
        format.json { render :show, status: :created, location: @fuel }
      else
        format.html { render :new }
        format.json { render json: @fuel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fuels/1
  # PATCH/PUT /fuels/1.json
  def update
    respond_to do |format|
      if @fuel.update(fuel_params)
        format.html { redirect_to @fuel, notice: 'atualizado.' }
        format.json { render :show, status: :ok, location: @fuel }
      else
        format.html { render :edit }
        format.json { render json: @fuel.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @fuel.destroy
    respond_to do |format|
      format.html { redirect_to fuels_url, notice: 'excluído.' }
      format.json { head :no_content }
    end
  end

  private
    def set_fuel
      @fuel = Fuel.find(params[:id])
    end

    def fuel_params
      params.require(:fuel).permit(:nome, :valor, :tipo)
    end
end
