class DriversController < ApplicationController

  def index
    @drivers = Driver.all.order(id: :desc)
  end

  def show
    @fuel = Fuel.all

    @consts = Const.all

    @rents = Rent.all

    @driver = Driver.find(params[:id])

    @dias_mes = @driver.qtd_dias_semana * 4

    @horas_mes = @dias_mes * @driver.horas_dia

    @numero_corridas = @driver.city.tph * @horas_mes
    @numero_corridas = @numero_corridas.round

    @km_percorridos = @numero_corridas * (@driver.city.distancia_pax + @driver.city.distancia_corrida)
    @km_percorridos = @km_percorridos.round(2)

    @ganhos_mes = @numero_corridas * @driver.city.asp
    @ganhos_mes = @ganhos_mes.round(2)

    @despesas_combustivel = (@km_percorridos/@driver.consumo_medio_combustivel) * @driver.fuel.valor
    @despesas_combustivel = @despesas_combustivel.round(2)
    if @driver.carro_proprio == true
      @despesas_texto = 'Despesas com o carro (mês):'
      @despesas_carro = (@driver.const.ipva_dpvat_licenciamento.to_f + @driver.const.manutencao.to_f + @driver.const.depreciacao.to_f)

      if @driver.seguro == true
        @despesas_carro = (@driver.const.ipva_dpvat_licenciamento.to_f + @driver.const.manutencao.to_f + @driver.const.depreciacao.to_f + @driver.const.seguro.to_f)
      end
    else
      @despesas_texto = 'Despesas com aluguel(mês):'
      @despesas_carro = (@driver.rent.valor.to_f)
    end

    @despesas_carro = @despesas_carro.round(2)

    @alimentacao_mes = @dias_mes * @driver.custo_alimentacao_dia

    @taxa = @driver.city.taxa_99 * 0.1

    @total_liquido = @ganhos_mes - (@ganhos_mes * @taxa.to_f) - @despesas_combustivel - @despesas_carro - @alimentacao_mes
    
    render(:layout => "layouts/app")
  end

  def new
    @driver = Driver.new
    render(:layout => "layouts/app")
  end

  def edit
  end

  def create
    @driver = Driver.new(driver_params)
    respond_to do |format|
      if @driver.save
        format.html { redirect_to @driver, notice: 'salvo.' }
        format.json { render :show, status: :created, location: @driver }
      else
        format.html { render :new }
        format.json { render json: @driver.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @driver.update(driver_params)
        format.html { redirect_to @driver, notice: 'atualizado.' }
        format.json { render :show, status: :ok, location: @driver }
      else
        format.html { render :edit }
        format.json { render json: @driver.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @driver.destroy
    respond_to do |format|
      format.html { redirect_to drivers_url, notice: 'excluído.' }
      format.json { head :no_content }
    end
  end

  private
    def set_driver
      @driver = Driver.find(params[:id])
    end

    def driver_params
      params.require(:driver).permit(:maior_idade, :motorista_99, :qtd_dias_semana, :quais_dias_semana, :horas_dia, :carro_proprio, :consumo_medio_combustivel, :seguro, :custo_lavagem_mes, :custo_alimentacao_dia, :city_id, :fuel_id, :rent_id, :const_id, :seg, :ter, :qua, :qui, :sex, :sab, :dom)
    end
end
