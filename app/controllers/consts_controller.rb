class ConstsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_const, only: [:show, :edit, :update, :destroy]

  def index
    @consts = Const.all
  end

  def show
    @total_com_seguro = (@const.ipva_dpvat_licenciamento.to_f + @const.manutencao.to_f + @const.depreciacao.to_f + @const.seguro.to_f)/12
    @total_sem_seguro = (@const.ipva_dpvat_licenciamento.to_f + @const.manutencao.to_f + @const.depreciacao.to_f)/12

  end

  def new
    @const = Const.new
  end

  def edit
  end

  def create
    @const = Const.new(const_params)

    respond_to do |format|
      if @const.save
        format.html { redirect_to @const, notice: 'criado.' }
        format.json { render :show, status: :created, location: @const }
      else
        format.html { render :new }
        format.json { render json: @const.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @const.update(const_params)
        format.html { redirect_to @const, notice: 'atualizado.' }
        format.json { render :show, status: :ok, location: @const }
      else
        format.html { render :edit }
        format.json { render json: @const.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @const.destroy
    respond_to do |format|
      format.html { redirect_to consts_url, notice: 'excluído.' }
      format.json { head :no_content }
    end
  end

  private
    def set_const
      @const = Const.find(params[:id])
    end

    def const_params
      params.require(:const).permit(:ipva_dpvat_licenciamento, :manutencao, :depreciacao, :seguro, :total, :mensal)
    end
end
