Rails.application.routes.draw do
  resources :cities
  get 'home/index'
  get 'admin/index'

  root 'drivers#new'

  devise_for :users
  resources :consts
  resources :fuels
  resources :rents
  resources :drivers
end
