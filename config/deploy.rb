lock "3.11.0"

set :application, 'calc-99-pop'
set :repo_url, 'git@bitbucket.org:thaisespindola/calc-99-pop.git'

set :deploy_to, '/var/www/calc-99-pop'

set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
end

namespace :db do
  desc 'Run db:migrate task'
  task :migrate do
    on primary(:app) do
      within "#{current_path}" do
        with rails_env: "#{fetch(:stage)}" do
          execute :rake, "db:migrate"
        end
      end
    end
  end
end