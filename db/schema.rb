# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_28_173021) do

  create_table "cities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "taxa_99"
    t.float "tph"
    t.float "tarifa_base"
    t.float "rs_km"
    t.float "rs_min"
    t.float "tarifa_minima"
    t.integer "tempo_medio_corrida"
    t.float "distancia_pax"
    t.float "distancia_corrida"
    t.float "asp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "consts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.float "manutencao"
    t.float "depreciacao"
    t.float "seguro"
    t.float "total"
    t.float "mensal"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "ipva_dpvat_licenciamento"
  end

  create_table "drivers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "maior_idade"
    t.boolean "motorista_99"
    t.string "celular"
    t.string "email"
    t.integer "qtd_dias_semana"
    t.integer "horas_dia"
    t.boolean "carro_proprio"
    t.integer "consumo_medio_combustivel"
    t.boolean "seguro"
    t.integer "custo_lavagem_mes"
    t.integer "custo_alimentacao_dia"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "city_id"
    t.bigint "fuel_id"
    t.boolean "seg"
    t.boolean "ter"
    t.boolean "qua"
    t.boolean "qui"
    t.boolean "sex"
    t.boolean "sab"
    t.boolean "dom"
    t.bigint "rent_id"
    t.bigint "const_id"
    t.string "ddd"
    t.index ["city_id"], name: "index_drivers_on_city_id"
    t.index ["const_id"], name: "index_drivers_on_const_id"
    t.index ["fuel_id"], name: "index_drivers_on_fuel_id"
    t.index ["rent_id"], name: "index_drivers_on_rent_id"
  end

  create_table "fuels", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.float "valor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tipo"
  end

  create_table "rents", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "nome"
    t.float "valor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "drivers", "cities"
  add_foreign_key "drivers", "consts"
  add_foreign_key "drivers", "fuels"
  add_foreign_key "drivers", "rents"
end
