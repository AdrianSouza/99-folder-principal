class RemoveIpvaFromConst < ActiveRecord::Migration[5.2]
  def change
    remove_column :consts, :ipva_dpvat_lic, :string
    add_column :consts, :ipva, :string
    add_column :consts, :dpvat, :string
    add_column :consts, :licenciamento, :string
  end
end
