class AddConstToDriver < ActiveRecord::Migration[5.2]
  def change
    add_reference :drivers, :const, foreign_key: true
  end
end
