class CreateRents < ActiveRecord::Migration[5.2]
  def change
    create_table :rents do |t|
      t.string :nome
      t.float :valor

      t.timestamps
    end
  end
end
