class AddIpvaToConst < ActiveRecord::Migration[5.2]
  def change
    remove_column :consts, :ipva, :string
    remove_column :consts, :dpvat, :string
    remove_column :consts, :licenciamento, :string
    add_column :consts, :ipva_dpvat_licenciamento, :string
  end
end
