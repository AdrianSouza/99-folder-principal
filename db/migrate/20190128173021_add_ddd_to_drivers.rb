class AddDddToDrivers < ActiveRecord::Migration[5.2]
  def change
    add_column :drivers, :ddd, :string
  end
end
