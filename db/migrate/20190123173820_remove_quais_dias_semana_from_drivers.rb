class RemoveQuaisDiasSemanaFromDrivers < ActiveRecord::Migration[5.2]
  def change
    remove_column :drivers, :quais_dias_semana, :string
    add_column :drivers, :seg, :boolean
    add_column :drivers, :ter, :boolean
    add_column :drivers, :qua, :boolean
    add_column :drivers, :qui, :boolean
    add_column :drivers, :sex, :boolean
    add_column :drivers, :sab, :boolean
    add_column :drivers, :dom, :boolean
  end
end
