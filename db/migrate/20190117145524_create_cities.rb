class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :name
      t.float :taxa_99
      t.float :tph
      t.float :tarifa_base
      t.float :rs_km
      t.float :rs_min
      t.float :tarifa_minima
      t.integer :tempo_medio_corrida
      t.float :distancia_pax
      t.float :distancia_corrida
      t.float :asp

      t.timestamps
    end
  end
end
