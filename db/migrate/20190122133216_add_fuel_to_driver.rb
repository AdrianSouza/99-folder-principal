class AddFuelToDriver < ActiveRecord::Migration[5.2]
  def change
    add_reference :drivers, :fuel, foreign_key: true
  end
end
