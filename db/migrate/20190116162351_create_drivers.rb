class CreateDrivers < ActiveRecord::Migration[5.2]
  def change
    create_table :drivers do |t|
      t.boolean :maior_idade
      t.boolean :motorista_99
      t.string :celular
      t.string :email
      t.integer :qtd_dias_semana
      t.string :quais_dias_semana
      t.integer :horas_dia
      t.boolean :carro_proprio
      t.integer :consumo_medio_combustivel
      t.boolean :seguro
      t.integer :custo_lavagem_mes
      t.integer :custo_alimentacao_dia

      t.timestamps
    end
  end
end
