class AddRentToDriver < ActiveRecord::Migration[5.2]
  def change
    add_reference :drivers, :rent, foreign_key: true
  end
end
