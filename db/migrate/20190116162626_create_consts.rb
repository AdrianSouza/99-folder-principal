class CreateConsts < ActiveRecord::Migration[5.2]
  def change
    create_table :consts do |t|
      t.float :ipva_dpvat_lic
      t.float :manutencao
      t.float :depreciacao
      t.float :seguro
      t.float :total
      t.float :mensal

      t.timestamps
    end
  end
end
