class ChangeTaxaType < ActiveRecord::Migration[5.2]
  def self.up
    change_table :cities do |t|
      t.change :taxa_99, :string
    end
  end

  def self.down
    change_table :cities do |t|
      t.change :taxa_99, :float
    end
  end
end
